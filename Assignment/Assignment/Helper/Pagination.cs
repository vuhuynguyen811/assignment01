﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Helper
{
    public static class Pagination
    {

		public static PagedData<T> PagedResult<T>(List<T> list, int PageNumber, int Totalpage) where T : class
		{
			// T : generic class
			// this : extention method
			// where T : class
			var result = new PagedData<T>();
			result.Data = list;
			result.TotalPages = Convert.ToInt32(Math.Ceiling((double)Totalpage / 5));
			result.CurrentPage = PageNumber;
			return result;
		}
	}
}
