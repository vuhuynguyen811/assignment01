﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment.Models;
using Microsoft.AspNetCore.Mvc;
using Assignment.Helper;
using Microsoft.EntityFrameworkCore;

namespace Assignment.Controllers
{
    public class StudentController : Controller
    {
		private readonly DBContext _context;

		public StudentController(DBContext context)
		{
			_context = context;
		}

		public IActionResult getStudent(int pageNumber)
		{
			List<Student> listData = _context.Students.Skip(5 * (pageNumber - 1)).Take(5).ToList(); //improved
			var Totalpage = _context.Students.Count();
			var pagedData = Pagination.PagedResult(listData, pageNumber, Totalpage);
			return Json(pagedData);
		}

		public IActionResult Index()
		{
			return View();
		}

		//public IActionResult Create()
		//{
		//	return View();
		//}

		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public async Task<IActionResult> Create([Bind("ID,LastName,FirstMidName,EnrollmentDate")] Student student)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		_context.Add(student);
		//		await _context.SaveChangesAsync();
		//		return RedirectToAction(nameof(getStudent));
		//	}
		//	return View(student);
		//}
	}
}