﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Assignment.Models;
using Microsoft.AspNetCore.Mvc;

namespace Assignment.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Students")]
    public class StudentController : Controller
    {
		private readonly DBContext _context;

		public StudentController(DBContext context)
		{
			_context = context;
		}

		// GET: api/Students
		[HttpGet]
		public IEnumerable<Student> GetStudents(int page)///int skip, int take)
		{
			return _context.Students;
		}
	}
}